import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // console.log(route.routeConfig?.path)
    if (localStorage.getItem('accessToken') && localStorage.getItem('refreshToken')) {
      if( route.routeConfig?.path && route.routeConfig.path == "moments") {
        return true;
      } else {
        this.router.navigate(['/moments/add'])
        return false;
      }
    } else {
      if( route.routeConfig?.path && route.routeConfig.path == "moments") {
        this.router.navigate(['/login'])
        return false;
      } else {
        return true;
      }
    }
  }

}
