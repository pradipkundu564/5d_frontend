import { EmailValidator } from "@angular/forms";


export interface FormFields { 
    label: string, 
    type: string, 
    formControl: string, 
    placeholder: string, 
    icon?: string, 
    required: boolean, 
    passEyeIcon?: string,
    requiredError?: string,
    hasPattern: boolean,
    patternError?: string
 }

 export interface SignUpData {
    firstName: string,
    lastName: string,
    mobile: string,
    email: string,
    city: string,
    password: string
 }

 export interface LogInData {
   email: string,
   password: string
}
