import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LogInData, SignUpData } from '../interfaces/form';

@Injectable({
  providedIn: 'root'
})
export class UserAuthService {

  constructor(private http: HttpClient) { }

  signUp(signUpData: SignUpData) {
    return this.http.post('http://localhost:3001/api/user', signUpData);
  }

  logIn(logInData: LogInData) {
    return this.http.post('http://localhost:3001/api/session', logInData);
  }
}
