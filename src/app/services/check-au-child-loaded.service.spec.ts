import { TestBed } from '@angular/core/testing';

import { CheckAuthChildLoadedService } from './check-auth-child-loaded.service';

describe('CheckAuthChildLoadedService', () => {
  let service: CheckAuthChildLoadedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CheckAuthChildLoadedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
