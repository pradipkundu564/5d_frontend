import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LogInData, SignUpData } from '../interfaces/form';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MomentsService {

  constructor(private http: HttpClient) { }

  getLocalStorageValue(key: string) {
    return localStorage.getItem(key);
  }

  signUp(signUpData: SignUpData) {
    return this.http.post('http://localhost:3001/api/moment', signUpData);
  }

  submitMoment(data: any) {
    const headers = new HttpHeaders()
      .set('authorization', 'Bearer ' + this.getLocalStorageValue('accessToken'))
      .set('x-refresh', '' + this.getLocalStorageValue('refreshToken'));
    console.log(data.tags)
    let formData: FormData = new FormData();
    formData.append('title', data.title);
    formData.append('tags', data.tags);
    for (var i = 0; i < data.files.length; i++) {
      formData.append("file", data.files[i]);
    }

    return this.http.post('http://localhost:3001/api/moment', formData,
      {
        headers: headers,
        //   reportProgress: true, 
        //   observe: 'events'
      }
    );
  }


  getMoments() {
    const headers = new HttpHeaders()
      .set('authorization', 'Bearer ' + this.getLocalStorageValue('accessToken'))
      .set('x-refresh', '' + this.getLocalStorageValue('refreshToken'));
    return this.http.get<Blob>('http://localhost:3001/api/moment', {
      headers: headers
    });
  }

  getImage(image: string): Observable<Blob> {
    console.log(image)
    const headers = new HttpHeaders()
      // .set('responseType', 'blob')
      .set('authorization', 'Bearer ' + this.getLocalStorageValue('accessToken'))
      .set('x-refresh', '' + this.getLocalStorageValue('refreshToken'));
    return this.http.get('http://localhost:3001/api/moment/image?image='+image, {
      headers: headers, responseType: 'blob'
    });
  }
}
