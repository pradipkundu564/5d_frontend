const pureEmail: RegExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
const phoneNumberInput: RegExp = /^(\+\d{1,3}[- ]?)?\d{10}$/
const allCountryPhoneNumber: RegExp = /(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\.]?[(]?[0-9]{1,3}[)]?([-\s\.]?[0-9]{3})([-\s\.]?[0-9]{3,4})/;

export const regexValidators = {
  phone: phoneNumberInput,
  allCountryPhoneNumber: allCountryPhoneNumber,
  email: pureEmail,
};