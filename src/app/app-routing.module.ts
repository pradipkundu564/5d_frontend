import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AddMomentComponent } from './components/moments/add-moment/add-moment.component';
import { ListMomentComponent } from './components/moments/list-moment/list-moment.component';
import { AuthComponent } from './components/auth/auth.component'
import { LogInComponent } from './components/auth/log-in/log-in.component';
import { SignUpComponent } from './components/auth/sign-up/sign-up.component';
import { AuthGuard } from './guards/auth.guard';
import { MomentsComponent } from './components/moments/moments.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {
    path: '',
    canActivate: [AuthGuard],
    component: AuthComponent,
    children: [
      {
        path: 'login',
        // canActivate: [AuthGuard],
        component: LogInComponent
      },
      {
        path: 'signup',
        // canActivate: [AuthGuard],
        component: SignUpComponent
      }
    ]
  },
  {
    path: 'moments',
    canActivate: [AuthGuard],
    component: MomentsComponent,
    children: [
      {
        path: 'add',
        // canActivate: [AuthGuardService],
        component: AddMomentComponent
      },
      {
        path: 'list',
        // canActivate: [AuthGuardService],
        component: ListMomentComponent
      }
    ]
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
