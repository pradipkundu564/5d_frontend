import { Component, OnInit } from '@angular/core';
import { MomentsService } from 'src/app/services/moments.service';
import { DomSanitizer } from '@angular/platform-browser';
import { GetMoments } from 'src/app/interfaces/moment';

@Component({
  selector: 'app-list-moment',
  templateUrl: './list-moment.component.html',
  styleUrls: ['./list-moment.component.scss']
})
export class ListMomentComponent implements OnInit {

  page = 1;
  pageSize = 4;
  collectionSize: number = 0;
  currentRate = 8;
  datas: GetMoments[] = [];
  allMoments: any = [];
  image: any;
  showList: boolean = false;

  constructor(private momentsService: MomentsService, private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.momentsService.getMoments().subscribe((data: any) => {
      console.log(data);
      this.collectionSize = data.length;
      this.datas = data;
      this.allMoments = data;
      this.datas.forEach((element: any, i: number) => {
        if (element.images.length > 0) {
          this.momentsService.getImage(element.images[0]).subscribe((imageRes: Blob) => {
            let objectURL = URL.createObjectURL(imageRes);
            let image = this.sanitizer.bypassSecurityTrustUrl(objectURL);
            element.image = image;
            if (i == data.length - 1) {
              this.showList = true;
            }
          })
        }
      });
    })

  }

}
