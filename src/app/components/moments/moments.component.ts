import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav/drawer';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-moments',
  templateUrl: './moments.component.html',
  styleUrls: ['./moments.component.scss']
})
export class MomentsComponent implements OnInit {
  panelOpenState: boolean = true;
  selectedMenu: string="";

  headerTitle: any = {
    add: 'Add new moment',
    list: 'Moments'
  };

  constructor(private router: Router, private route: ActivatedRoute) { }
  innerWidth: any;
  ngOnInit(): void {
    this.innerWidth = window.innerWidth;
    if(this.router.url.includes('add')) {
      this.selectedMenu = 'add';
    } else {
      this.selectedMenu = 'list';
    }
    
  }
  @ViewChild('drawer')
  public drawer!: MatDrawer;
  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    // console.log('screen', window.innerWidth);
    this.innerWidth = window.innerWidth;
    // console.log(this.drawer);
    if(window.innerWidth <= 1130){
      this.drawer.close();
    }
    if(window.innerWidth > 1130){
      this.drawer.open();
    }
  }

  chageMoment(type: string) {
    this.selectedMenu = type;
    if(type == 'add') {
      this.router.navigate(['/moments/add'])
    } else {
      this.router.navigate(['/moments/list'])
    }
  }


}
