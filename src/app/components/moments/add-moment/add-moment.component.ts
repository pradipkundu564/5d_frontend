import { AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MomentsService } from 'src/app/services/moments.service';

export interface Tag {
  name: string;
}

@Component({
  selector: 'app-add-moment',
  templateUrl: './add-moment.component.html',
  styleUrls: ['./add-moment.component.scss']
})
export class AddMomentComponent implements OnInit, AfterViewInit {
  deleteFileCheck: boolean = false;
  error: string | undefined;

  constructor(private renderer: Renderer2, private momentsService: MomentsService) { }
  @ViewChild('inputDiv')
  inputDiv!: ElementRef;

  @ViewChild('chipDiv')
  chipDiv!: ElementRef;
  addMomentForm: FormGroup = new FormGroup({
    title: new FormControl('', [Validators.required])
  });
  ngOnInit(): void {
    

  }

  ngAfterViewInit(): void {
  }


  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  tags: Tag[] = [];

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our tag
    if (value) {
      this.tags.push({ name: value });
    }

    // Clear the input value
    event.chipInput!.clear();
    // console.log(this.inputDiv.nativeElement.firstChild, this.chipDiv)
    this.chipDiv.nativeElement.childNodes.forEach((element: any) => {
      this.renderer.insertBefore(this.inputDiv.nativeElement, element, this.inputDiv.nativeElement.firstChild)
    });
    // this.renderer.insertBefore(this.inputDiv.nativeElement, this.chipDiv.nativeElement, this.inputDiv.nativeElement.firstChild)
  }

  remove(tag: Tag): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }

  @ViewChild("fileDropRef", { static: false })
  fileDropEl!: ElementRef;
  files: any[] = [];

  /**
   * on file drop handler
   */
  onFileDropped($event: any[]) {
    this.prepareFilesList($event);
  }

  /**
   * handle file from browsing
   */
  fileBrowseHandler($event: any) {
    this.prepareFilesList($event.target.files);
  }

  /**
   * Delete file from files list
   * @param index (File index)
   */
  deleteFile(index: number) {
    // this.files.forEach(file=>{
    //   console.log(file)
    //   if (file.progress < 100) {
    //     console.log("Upload in progress.");
    //     this.deleteFileCheck = false;
    //     return;
    //   }
    //   this.deleteFileCheck = true;
    // })
    if (this.deleteFileCheck) this.files.splice(index, 1);
  }

  /**
   * Simulate the upload process
   */
  // uploadFilesSimulator(index: number) {
  //   setTimeout(() => {
  //     if (index === this.files.length) {
  //         this.deleteFileCheck = true;
        
  //       return;
  //     } else {
  //       const progressInterval = setInterval(() => {
  //         if (this.files[index].progress === 100) {
  //           clearInterval(progressInterval);
  //           this.uploadFilesSimulator(index + 1);
  //           this.files.forEach(file => {
  //             console.log(file)
  //             if (file.progress < 100) {
  //               console.log("Upload in progress.");
  //               this.deleteFileCheck = false;
  //               return;
  //             }
  //           })
  //         } else {
  //           this.files[index].progress += 5;
  //         }
  //       }, 200);
  //     }
  //   }, 1000);
  // }

  /**
   * Convert Files list to normal array list
   * @param files (Files List)
   */
  prepareFilesList(files: Array<any>) {
    for (const item of files) {
      item.progress = 0;
      this.files.push(item);
    }
    this.fileDropEl.nativeElement.value = "";
    // this.uploadFilesSimulator(0);
  }

  /**
   * format bytes
   * @param bytes (File size in bytes)
   * @param decimals (Decimals point)
   */
  formatBytes(bytes: number, decimals = 2) {
    if (bytes === 0) {
      return "0 Bytes";
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  }

  submitMoment() {
    console.log(this.files)
    console.log(this.addMomentForm.controls.title.value);
    let tagData:string[] = this.tags.map(tag=> {
      return tag.name;
    })
    this.momentsService.submitMoment({files: this.files, tags: tagData, title: this.addMomentForm.controls.title.value}).subscribe(
      data => {
        this.files = [];
        this.addMomentForm.controls.title.setValue('');
        this.tags = [];
        this.error = "";
      },
      error=> {
          this.error = "Something wrong happened!";
      }
    )
  }
}
