import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  links: any = {
    signup: {
      headerFirst: 'Sign In',
      headerSecond: 'To start using the app',
      first: 'Not a member?',
      second: 'Sign Up'
    },
    login: {
      headerFirst: 'Sign Up',
      headerSecond: 'To be a member',
      first: 'Already a member?',
      second: 'Sign In'
    }
  }
  notActive: string;
  activated: string = "";

  constructor(private router: Router) {
    this.router.url == '/login' ? this.notActive = 'signup' : this.notActive = 'login'
   }

  ngOnInit(): void {
    if(this.router.url == '/login') {
      this.activated = 'login'
    } else if(this.router.url == '/signup') {
      this.activated = 'signup';
    }
  }

  changeAuth() {
    console.log('router', this.router.url)
    this.router.url == '/login' ? this.notActive = 'login' : this.notActive = 'signup'
    this.router.url == '/login' ? this.router.navigate(['/signup']) : this.router.navigate(['/login'])
  }

  onActivate(event: Event) {
    // console.log(event);
  }

  onDeactivate(event: Event) {
    // console.log(event);
  }
}
