import { AfterContentChecked, AfterViewInit, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormFields, LogInData } from 'src/app/interfaces/form';
import { regexValidators } from 'src/app/validator';
import { UserAuthService } from '../../../services/user-auth.service'

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {

  logInForm: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.pattern(regexValidators.email)]),
    password: new FormControl('', [Validators.required])
  });

  formFields: FormFields[] = [
    { label: "Enter email id", type: "text", formControl: "email", placeholder: "ministry@government.in", icon: "email", required: true, requiredError: "** This field is required!", hasPattern: true, patternError: "** Invalid email!" },
    { label: "Enter Password", type: "password", formControl: "password", placeholder: "Password", icon: "lock", required: true, passEyeIcon: "visibility", requiredError: "** This field is required!", hasPattern: false }
  ]
  error: string = "";
  constructor(private router: Router, private userAuthService: UserAuthService) {
  }



  ngOnInit(): void {
  }


  logIn() {
    let formData: any = this.logInForm.controls;
    let data: LogInData = {
      email: formData.email.value,
      password: formData.password.value
    }
    // console.log(data)
    this.userAuthService.logIn(data).subscribe((data: any) => {
      // console.log(data);
      localStorage.setItem('accessToken', data.accessToken);
      localStorage.setItem('refreshToken', data.refreshToken);
      this.router.navigate(['/moments/add']);
    },
      error => {
        this.error = error.error[0];
        setTimeout(() => {
          this.error = "";
        }, 6000);
      }
    )
  }

  showPassword(index: number) {
    this.formFields[index].type == 'text' ? this.formFields[index].type = 'password' : this.formFields[index].type = 'text';
    this.formFields[index].passEyeIcon == 'visibility' ? this.formFields[index].passEyeIcon = 'visibility_off' : this.formFields[index].passEyeIcon = 'visibility';
  }
}
