import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormFields, SignUpData } from 'src/app/interfaces/form';
import { regexValidators } from 'src/app/validator';
import { UserAuthService } from '../../../services/user-auth.service'

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  signUpForm: FormGroup = new FormGroup({
    fName: new FormControl('', [Validators.required]),
    lName: new FormControl('', [Validators.required]),
    mobile: new FormControl('', [Validators.required, Validators.pattern(regexValidators.phone)]),
    email: new FormControl('', [Validators.required, Validators.pattern(regexValidators.email)]),
    city: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });

  formFields: FormFields[] = [
    { label: "First Name", type: "text", formControl: "fName", placeholder: "First Name", icon: "person_outline", required: true, requiredError: "** This field is required!", hasPattern: false },
    { label: "Last Name", type: "text", formControl: "lName", placeholder: "Last Name", icon: "person_outline", required: true, requiredError: "** This field is required!", hasPattern: false },
    { label: "Mobile No.", type: "tel", formControl: "mobile", placeholder: "", icon: "", required: true, requiredError: "** This field is required!", hasPattern: true, patternError: "** Invalid number!" },
    { label: "Enter email id", type: "text", formControl: "email", placeholder: "ministry@government.in", icon: "email", required: true, requiredError: "** This field is required!", hasPattern: true, patternError: "** Invalid email!" },
    { label: "City", type: "text", formControl: "city", placeholder: "city", icon: "", required: true, requiredError: "** This field is required!", hasPattern: false },
    { label: "Enter Password", type: "password", formControl: "password", placeholder: "password", icon: "lock", passEyeIcon: "visibility", required: true, requiredError: "** This field is required!", hasPattern: false }
  ]

  formFieldsCalculation: number = 0;
  phoneNumber: string = "";
  dialCode: string = "";
  error: string = "";
  message: string  = "";
  constructor(private router: Router, private userAuthService: UserAuthService) {
  }

  ngOnInit(): void {
  }

  showPassword(index: number) {
    this.formFields[index].type == 'text' ? this.formFields[index].type = 'password' : this.formFields[index].type = 'text';
    this.formFields[index].passEyeIcon == 'visibility' ? this.formFields[index].passEyeIcon = 'visibility_off' : this.formFields[index].passEyeIcon = 'visibility';
  }

  signUp() {
    let formData: any = this.signUpForm.controls;
    this.phoneNumber = '+'+this.dialCode+formData.mobile.value;
    // console.log(formData)
    let data: SignUpData = {
      firstName: formData.fName.value,
      lastName: formData.lName.value,
      mobile: this.phoneNumber,
      email: formData.email.value,
      city: formData.city.value,
      password: formData.password.value
    }
    // console.log(data);

    this.userAuthService.signUp(data).subscribe(data => {
      this.message = "Account created";
      this.signUpForm.reset();
      setTimeout(() => {
        this.message = "";
      }, 6000);
    },
      error => {
        this.error = error.error[0];
        setTimeout(() => {
          this.error = "";
        }, 6000);
      }
    )

  }

  createRange(number: number) {
    this.formFields.length % 2 == 0 ? this.formFieldsCalculation = this.formFields.length / 2 : this.formFieldsCalculation = Math.floor(this.formFields.length / 2) + 1;
    return new Array(this.formFieldsCalculation);
  }

  hasError(event: any) {
    // console.log('error', event);
    // event ? "" : this.phoneNumber = "";
    // console.log('number', this.phoneNumber);
  }

  getNumber(event: any) {
    // console.log('n 2', this.phoneNumber)
    // if (regexValidators.allCountryPhoneNumber.test(event)) {
    //   this.phoneNumber = event;
    //   console.log('n', this.phoneNumber)
    // }

  }

  telInputObject(event: any) {
    this.dialCode = event.s.dialCode;
  }

  onCountryChange(event: any) {
    this.dialCode = event.dialCode;
  }
}
